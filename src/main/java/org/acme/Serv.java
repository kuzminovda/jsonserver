package org.acme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import student.Student;


public class Serv
{

    private static Socket           clientSocket;   // Сокет клиента для получения клиентских потоков
    private static ServerSocket     server;         // Серверный сокет для обслуживания клиентсикх сокетов
    private static InputStream      in;             // Поток чтения из сокета
    private static OutputStream     out;            // Поток записи в сокет  
    private static Scanner          sc; 
    

    private static int    serverTcpPort = 7777;
    private static String hellow        = "Сервер подключен";
   
    private static int      clientPort;
    private static String   clientAddress;
    private static String   clientHost;
    

    
    
    
    public static void main(String[] args) throws IOException
    {

        try
        {
            server = new ServerSocket(serverTcpPort); // создание сервер

            System.out.println("Сервер TCP стартовал");
            System.out.println("IP адрес сервера:  [" + server.getInetAddress().getHostAddress() + "]");
            System.out.println("Имя узла сервера:  [" + server.getInetAddress().getHostName() + "]");
            System.out.println("TCP порт сервера:  [" + serverTcpPort + "]");
            System.out.println("Ожидание подключения клиента...");
            System.out.println("");
            clientSocket = server.accept(); // Обработка подключения клиента и получение доступа 
            // к клиентскому сокету

            in = clientSocket.getInputStream();     // Получение доступа к потоку ввода клиентского сокета
            out = clientSocket.getOutputStream();   // Получение доступа к потоку вывода клиентского сокета

            out.write(hellow.getBytes());   // Заполнение  буфера отправки
            out.write("\n".getBytes());     //
            out.flush();                    // Отправка  частично зполненный буфер через сетевое соединение

            processClient(clientSocket,in,out);
            
        } catch (IOException e)
        {
            System.out.println("Ошибка ввода-вывода сетевого соединения");
            System.out.println("Описание ошибки: {"+e.getLocalizedMessage()+"}");
            
        } finally
        {
            System.out.println("");
            System.out.println("SERVER: Завершение работы сервера...");
            if (clientSocket != null  && !clientSocket.isClosed())
            {
                clientSocket.close();
            }
            if (server != null)
            {
                server.close();
            }
            if (in != null)
            {
                in.close();
            }
            if (out != null)
            {
                in.close();
            }
            System.out.println("SERVER: Работа сервера завершена");

        }
    }
    
    public static void processClient(Socket clientSocket,InputStream in,  OutputStream  out) throws JsonProcessingException, IOException
    {
        sc = new Scanner(in);  // Скарование строк от клиента 
        PrintWriter toClient    = new PrintWriter(out);
        String msg = "";
        
        
            
        // Получение параметров подключения клиента
        clientPort       = clientSocket.getPort();
        clientAddress    = clientSocket.getLocalAddress().getHostAddress();
        clientHost       = clientSocket.getLocalAddress().getCanonicalHostName();
        
        
        msg = String.format("==> Подключен клиент: %-20s %-20s %-7s", "["+clientHost, clientAddress.toString(),""+clientPort+"]");
        System.out.println(msg);
        
        // Цикл чтения строк от клиента
        while (sc.hasNextLine())
        {
            String clientmsg = sc.nextLine();// Чтение строки от клиента
            
            ObjectMapper objectMapper = new ObjectMapper();
            Student s1 = objectMapper.readValue(clientmsg, Student.class);
            objectMapper.writeValue(new File("target/s1.json"), s1);
            
            System.out.println("SERVER: Получен и сохранен объект Student: " + s1);
            
            
            // Форматирование строки для отображения параметров и тестового сообщения от клиента 
            //
            //                             host  ip   port      clientmsg
            //                              |     |    |          |
            msg = String.format("SERVER: %-15s %-15s %-7s %-5s %-120s", "["+clientHost, clientAddress.toString(),""+clientPort+"]", "==>" ,clientmsg);
            
            System.out.println(msg);          // Печать строки от клиента на сервере  
            toClient.println(msg);            // Отправка текста от клиента клиенту
            
            if (clientmsg.equalsIgnoreCase("exit"))
            {
                String client = String.format("%-20s %-20s %-7s", "["+clientHost, clientAddress.toString(),""+clientPort+"]");
                msg = "SERVER: Получена команда завершения работы от клиента "+client+"";
                
                break;
            }
            
        }  
        closeClientConnection(); // Завершение работы сервера
       
        
        
    }
    
    private static void closeClientConnection()
    {
        System.out.println("SERVER: Закрытие клиентского соединения....");
        try
        {
            if (sc != null )  sc.close();
            if (out != null ) out.close();
            if (in != null )  in.close();
            if (clientSocket != null && !clientSocket.isClosed()) {clientSocket.close(); }
            
            String msg = String.format("==> Отключен  клиент:  %-20s %-20s %-7s", "["+clientHost, clientAddress.toString(),""+clientPort+"]");
            System.out.println(msg);
        } catch (IOException e)
        {
             System.out.println("SERVER: Ошибка сетевого соединения при завершении работы: {"+e.getLocalizedMessage()+"}");
        }
        
    }
    
    
}
