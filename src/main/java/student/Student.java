
package student;

/**
 *
 * @author kuzminovda
 */
public class Student {
    
    private String firstname;

    /**
     * Get the value of firstname
     *
     * @return the value of firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set the value of firstname
     *
     * @param firstname new value of firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

        private String lastname;

    /**
     * Get the value of lastname
     *
     * @return the value of lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Set the value of lastname
     *
     * @param lastname new value of lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

        private String kurs;

    /**
     * Get the value of kurs
     *
     * @return the value of kurs
     */
    public String getKurs() {
        return kurs;
    }

    /**
     * Set the value of kurs
     *
     * @param kurs new value of kurs
     */
    public void setKurs(String kurs) {
        this.kurs = kurs;
    }

        private String gruppa;

    /**
     * Get the value of gruppa
     *
     * @return the value of gruppa
     */
    public String getGruppa() {
        return gruppa;
    }

    /**
     * Set the value of gruppa
     *
     * @param gruppa new value of gruppa
     */
    public void setGruppa(String gruppa) {
        this.gruppa = gruppa;
    }

}
